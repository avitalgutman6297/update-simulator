export type Link = {
    linkType: 'oneToMany' | 'oneToOne',
    entity: string
} 

export type FieldType = 'string' | 'number' | 'boolean' | Link

export type EntityType = {
    name: string
    fields: Record<string, FieldType>
}

export type FieldUpdateDefinition = {
    fieldName: string
}

export type EntityUpdateDefinition = {
    entityType: string,
    fieldUpdateDefinitions: FieldUpdateDefinition[],
}

export type Scenario = {
    name: string,
    updates: number,
    updateDefinition: EntityUpdateDefinition[],
    rootEntities: string[],
}

export type TestsConfiguration = {
    entitiesDefinitions: EntityType[]
    scenario: Scenario
}
