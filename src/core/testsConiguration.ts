import { readFileSync } from 'fs'
import { TestsConfiguration } from './types';

export const readTestConfiguration = (path: string): TestsConfiguration => {
    const content = JSON.parse(readFileSync(path).toString());

    return content as TestsConfiguration;
}
