import { model } from "mongoose";
import { DbAccessObjects } from "./dbAccessProvider";
import { TestsConfiguration } from "./types";
import { TYPENAME_TO_RANDOMIZER } from "./utils";

async function updateField(originalEntity: any, field: string, entityTypeName: string, testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) {
    const entityDefinition = testsConfiguration.entitiesDefinitions.filter(entityType => entityType.name === entityTypeName)[0]
    const fieldType = entityDefinition.fields[field];

    if (typeof fieldType === 'string') {
        return TYPENAME_TO_RANDOMIZER[fieldType]();
    } else if (typeof fieldType === 'object') {
        if (fieldType.linkType === 'oneToOne') {
            const originalLink = await entityDbAccess[fieldType.entity].model.findById(originalEntity[field].toString()).exec();
            return await updateEntity(originalLink, fieldType.entity, testsConfiguration, entityDbAccess);
        } else {
            const originalLinks: any[] = await entityDbAccess[fieldType.entity].model.find().where("_id").in(originalEntity[field]).exec();
            const updatePromises = originalLinks.map(link => updateEntity(link, fieldType.entity, testsConfiguration, entityDbAccess));
            return await Promise.all(updatePromises);
        }
    }

    throw new Error(`No schema mapping found for fieldType ${JSON.stringify(fieldType)}`)
}

const updateEntity = async (originalEntity: any, entityTypeName: string, testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) => {
    const entityDefinition = testsConfiguration.entitiesDefinitions.filter(entityType => entityType.name === entityTypeName)[0]

    const entity: Record<string, any> = {};
    for (let field of Object.keys(entityDefinition.fields)) {
        entity[field] = await updateField(originalEntity, field, entityTypeName, testsConfiguration, entityDbAccess);
    }
    return await entityDbAccess[entityTypeName].model.findByIdAndUpdate(originalEntity._id, entity).exec();
}

export const updateEntities = async (testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) => {
    for(let entityName of testsConfiguration.scenario.rootEntities) {
        const cursor = entityDbAccess[entityName].model.find({}).cursor();
        let i = 0;
        for (let originalEntity = await cursor.next(); originalEntity != null; originalEntity = await cursor.next()) {
            await updateEntity(originalEntity, entityName, testsConfiguration, entityDbAccess);
            console.log("------------------------")
            console.log({originalEntity})
            console.log({
                updated: await entityDbAccess[entityName].model.findById(originalEntity._id).exec()
            });
            console.log("------------------------")
            i++;
            console.log(i);
        }
    }
    console.log("Finished updating")
}