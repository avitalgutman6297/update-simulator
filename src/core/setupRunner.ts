import mongoose from "mongoose"

import { DbAccessObjects } from "./dbAccessProvider"
import { createEntities } from "./entitiesCreator"
import { updateEntities } from "./entitiesUpdater"
import { TestsConfiguration } from "./types"

export const runSetup = async (dbUrl: string, testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) => {
    await mongoose.connect(dbUrl);
    await createEntities(testsConfiguration, entityDbAccess);
    await updateEntities(testsConfiguration, entityDbAccess);
}