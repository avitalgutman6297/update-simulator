import mongoose, { SchemaDefinition } from "mongoose";

import { EntityType, FieldType } from "./types";

export type DbAccessObjects = {
    schema: mongoose.Schema,
    model: mongoose.Model<any>,
}

const TYPENAME_TO_TYPE = {
    'string': String,
    'number': Number,
    'boolean': Boolean
}


const testConfigTypeToMongoSchemaType = (fieldType: FieldType) => {
    if (typeof fieldType === 'string') {
        return TYPENAME_TO_TYPE[fieldType];
    } else if (typeof fieldType === 'object') {
        if (fieldType.linkType === 'oneToOne') {
            return {
                type: mongoose.Schema.Types.ObjectId,
                ref: fieldType.entity,
            }
        } else {
            return [
                {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: fieldType.entity,
                }
            ]
        }
    }

    throw new Error(`No schema mapping found for fieldType ${JSON.stringify(fieldType)}`)
}

export const createEntityDbObjects = (entityType: EntityType): DbAccessObjects => {
    const schemaFields: SchemaDefinition  = {}
    Object.keys(entityType.fields).forEach(key => {
        schemaFields[key] = testConfigTypeToMongoSchemaType(entityType.fields[key]);
    })

    const schema = new mongoose.Schema(schemaFields);
    const model = mongoose.model(entityType.name, schema);
    return {
        schema,
        model,
    }
}
