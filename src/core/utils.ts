export const randomBoolean = () => {
    return Math.random() > 0.5 ? true : false;
}

export const randomString = () => {
    return (Math.random() + 1).toString(36).substring(7);
}

export const randomNumber = () => {
    return Math.random();
}

export const TYPENAME_TO_RANDOMIZER = {
    'string': randomString,
    'number': randomNumber,
    'boolean': randomBoolean
}
