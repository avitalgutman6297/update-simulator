import { DbAccessObjects } from "./dbAccessProvider";
import { TestsConfiguration } from "./types";
import { TYPENAME_TO_RANDOMIZER } from "./utils";

async function fillField(field: string, entityTypeName: string, testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) {
    const entityDefinition = testsConfiguration.entitiesDefinitions.filter(entityType => entityType.name === entityTypeName)[0]
    const fieldType = entityDefinition.fields[field];

    if (typeof fieldType === 'string') {
        return TYPENAME_TO_RANDOMIZER[fieldType]();
    } else if (typeof fieldType === 'object') {
        if (fieldType.linkType === 'oneToOne') {
            return await createEntity(fieldType.entity, testsConfiguration, entityDbAccess);
        } else {
            return [await createEntity(fieldType.entity, testsConfiguration, entityDbAccess), await createEntity(fieldType.entity, testsConfiguration, entityDbAccess)]
        }
    }

    throw new Error(`No schema mapping found for fieldType ${JSON.stringify(fieldType)}`)
}

const createEntity = async (entityTypeName: string, testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) => {
    const entityDefinition = testsConfiguration.entitiesDefinitions.filter(entityType => entityType.name === entityTypeName)[0]

    const entity: Record<string, any> = {};
    for (let field of Object.keys(entityDefinition.fields)) {
        entity[field] = await fillField(field, entityTypeName, testsConfiguration, entityDbAccess);
    }

    return await entityDbAccess[entityTypeName].model.create(entity);
}

export const createEntities = async (testsConfiguration: TestsConfiguration, entityDbAccess: Record<string, DbAccessObjects>) => {
    for(let entityName of testsConfiguration.scenario.rootEntities) {
        for(let i = 0; i < testsConfiguration.scenario.updates; i++) {
            await createEntity(entityName, testsConfiguration, entityDbAccess);
        }
    }
}
