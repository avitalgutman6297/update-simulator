import { initConnection } from "./connectionProvider";
import { createEntityDbObjects, DbAccessObjects } from "./core/dbAccessProvider";
import { runSetup } from "./core/setupRunner";
import { readTestConfiguration } from "./core/testsConiguration";
import { TestsConfiguration } from "./core/types";

const main = async () => {
    const uri = await initConnection()
    console.log(`Connected to ${uri}`);

    const testsConfiguration: TestsConfiguration = readTestConfiguration('./example-config.json');
    console.log('using the following configuration to run performance tests')
    console.log(testsConfiguration);

    const entityDbAccess: Record<string, DbAccessObjects> = {}
    for (let entityDefinition of testsConfiguration.entitiesDefinitions) {
        entityDbAccess[entityDefinition.name] = createEntityDbObjects(entityDefinition);
    }

    await runSetup(uri, testsConfiguration, entityDbAccess);
}

main();