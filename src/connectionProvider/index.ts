import { MongoMemoryServer } from 'mongodb-memory-server';

export const initConnection = async () : Promise<string> => {
    const mongod = await MongoMemoryServer.create();

    const cleanup = (mongod: MongoMemoryServer | undefined) => {
        if (mongod) {
            console.log("Stopping mongod");
            mongod.stop();
        }
    }

    process.on('exit', () => {
        console.log('exit')
        cleanup(mongod);
    });

    process.on('SIGINT', () => {
        console.log('SIGINT')
        cleanup(mongod);
    })

    process.on('SIGABRT', () => {
        console.log('SIGABRT')
        cleanup(mongod);
    })

    return mongod.getUri();
}